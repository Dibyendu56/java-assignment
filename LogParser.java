import java.util.*;
import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;

class ArraySort implements Comparator<int[]>{
    public int compare(final int[] entry1, final int[] entry2) {
        if (entry1[5] > entry2[5]){
            return 1;
        }
        else{
            return -1;
        }
    }
}

class Main{
    private String path;
    private String flag=null;
    private ArrayList<ArrayList<String>> urlList = new ArrayList<ArrayList<String>> ();
    private int [][] logStore = new int[24][6];
    private HashMap<Integer,String> hmap = new HashMap<Integer,String>();

	Main(String path, String optflag){
        this.path = path;
        flag = optflag;
        initVariable();
    }
	
    Main(String path){
        this.path = path;
        initVariable();
    }

    private void initVariable(){
        for(int index=0; index<24; index++){
            urlList.add(new ArrayList<String>());
            logStore[index][0] = index;
        }
        hmap.put(0, "12.00 am - 1.00 am"); hmap.put(1, "1.00 am - 2.00 am"); hmap.put(2, "2.00 am - 3.00 am");
        hmap.put(3, "3.00 am - 4.00 am"); hmap.put(4, "4.00 am - 5.00 am"); hmap.put(5, "5.00 am - 6.00 am");
        hmap.put(6, "6.00 am - 7.00 am"); hmap.put(7, "7.00 am - 8.00 am"); hmap.put(8, "8.00 am - 9.00 am");
        hmap.put(9, "9.00 am - 10.00 am"); hmap.put(10, "10.00 am - 11.00 am"); hmap.put(11, "11.00 am - 12.00 pm");
        hmap.put(12, "12.00 pm - 1.00 pm"); hmap.put(13, "1.00 pm - 2.00 pm"); hmap.put(14, "2.00 pm - 3.00 pm");
        hmap.put(15, "3.00 pm - 4.00 pm"); hmap.put(16, "4.00 pm - 5.00 pm"); hmap.put(17, "5.00 pm - 6.00 pm");
        hmap.put(18, "6.00 pm - 7.00 pm"); hmap.put(19, "7.00 pm - 8.00 pm"); hmap.put(20, "8.00 pm - 9.00 pm");
        hmap.put(21, "9.00 pm - 10.00 pm"); hmap.put(22, "10.00 pm - 11.00 pm"); hmap.put(23, "11.00 pm - 12.00 pm");
    }

    private void calculateURI(String str, int id){
        String url = "";
        for(int index=5; index<str.length()-1;index++){
            url += str.charAt(index);
        }
        urlList.get(id).add(url);
    }

    private void calculateResponseTime(String str, int id){
        int responseTime = 0;
        for(int index=5; index<str.length(); index++){
            if(str.charAt(index)>='0' && str.charAt(index)<='9'){
                responseTime = responseTime*10 + ((int) str.charAt(index) - 48);
            }
            else{
                break;
            }
        }
        logStore[id][4] += responseTime;
    }

    private void countGET(int id){
        logStore[id][1]++;
    }

    private void countPOST(int id){
        logStore[id][2]++;
    }

    private void storeTotalGetPost(){
        for(int index=0; index<24; index++){
            logStore[index][5] = logStore[index][1] + logStore[index][2];
        }
    }

    void showDisplay(){
        calculateDisplay();
        storeTotalGetPost();

        for(int index=0; index<24; index++){
            HashSet<String> hset = new HashSet<String>();
            for(String str: urlList.get(index)){
                hset.add(str);
            }
            logStore[index][3] = hset.size();
        }
		
        if(flag=="sort"){
			ArraySort as = new ArraySort();
			Arrays.sort(logStore, as);
        }

        System.out.println("        Time"+"            "+"GET/POST Count"+"   "+"Unique URI Count"+
                "   "+"Total Response Time");
        for(int index=0; index<24; index++){
            for(int field=0; field<5; field++){
                if(field==0){
                    int id = logStore[index][field];
                    System.out.print(hmap.get(id)+"          ");
                }
                else if(field==1){
                    System.out.print(logStore[index][field]+"/"+logStore[index][field+1]+"           ");
                }
                else if(field==3){
                    System.out.print("       "+logStore[index][field]+"          ");
                }
                else if(field==4){
                    System.out.print("        "+logStore[index][field]+"          ");
                }

            }
            System.out.println();
        }
    }

    private void calculateDisplay(){
        try{
            File myObj = new File(path);
            Scanner myReader = new Scanner(myObj);
            int i=0;

            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                String[] tokens = data.split("[ ,]");
                String time = tokens[1];
                int id = 10 * ((int) time.charAt(1) - 48) + ((int) time.charAt(0) - 48);
                for(String str: tokens){
                    if(str.length()>2 && str.charAt(0)=='U' && str.charAt(1)=='R' && str.charAt(2)=='I') {
                        calculateURI(str, id);
                    }
                    if(str.length()>3 && str.charAt(0)=='t' && str.charAt(1)=='i' && str.charAt(2)=='m'
                            && str.charAt(3)=='e'){
                            calculateResponseTime(str,id);
                    }
                    if(str.length()==1 && str.charAt(0)=='G'){
                        countGET(id);
                    }
                    if(str.length()==1 && str.charAt(0)=='P'){
                        countPOST(id);
                    }
                }
            }
            myReader.close();
        } catch(FileNotFoundException e) {
            System.out.println("File Not Found");
        }

        System.out.println("Hello World");
    }
}

public class LogParser {

    public static void main(String[] args) {
		
		Main ob;
		
		if(args.length==2){
			ob = new Main(args[0], args[1]);
		}
		else{
			ob = new Main(args[0]);
		}
        
		ob.showDisplay();

    }
}
